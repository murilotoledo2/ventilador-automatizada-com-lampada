#include <Modbusino.h>

/* Initialize the slave with the ID 1 */
ModbusinoSlave modbusino_slave(1);
/* Allocate a mapping of 10 values */
uint16_t tab_reg[10];


int botao1 = 3;
int botao2 = 5;
int botao3 = 7;
int botao4 = 8;
int rele1 = 2;
int rele2 = 4;
int rele3 = 6;
int rele4 = 9;
int estado1 = 0;
int estado2 = 0;
int estado3 = 0;
int estado4 = 0;
int filtro1=100;
int filtro2=100;
int filtro3=100;
int filtro4=100;

int but1 = 0;
int but2 = 0;
int but3 = 0;
int but4 = 0;

int ver1 = 0;
int ver2 = 0;
int ver3 = 0;
int ver4 = 0;

void setup() {

  modbusino_slave.setup(9600);

  //inicializar rele
  digitalWrite(rele1, HIGH);
  digitalWrite(rele2, HIGH);
  digitalWrite(rele3, HIGH);
  digitalWrite(rele4, HIGH);

  // put your setup code here, to run once:
  pinMode(botao1, INPUT);
  pinMode(rele1, OUTPUT);
  pinMode(botao2, INPUT);
  pinMode(rele2, OUTPUT);
  pinMode(botao3, INPUT);
  pinMode(rele3, OUTPUT);
  pinMode(botao4, INPUT);
  pinMode(rele4, OUTPUT);
  
}

void loop() {


  
  
  //tab_reg[0]=0;
  //tab_reg[1]=0;
  //tab_reg[2]=0;
  //tab_reg[3]=0;

  but1 = tab_reg[0];
  but2 = tab_reg[1];
  but3 = tab_reg[2];
  but4 = tab_reg[3];
  
  
//Logica do Botao 1===========================================
  if (digitalRead(botao1) == 0 || but1 == 1)
  {    
      filtro1--;
      if(filtro1==0)
      {
        digitalWrite(rele2, HIGH);
        digitalWrite(rele3, HIGH);
        if(estado1==0)
        {
          if(ver1 == 0){
            //Condicional caso o botao 2 ou 3 estejam ativos
            if(estado2 == 1 || estado3 == 1)
            {
              //Desliga ambos
              estado2 = 0;
              estado3 = 0;
              digitalWrite(rele2, HIGH);
              digitalWrite(rele3, HIGH);
            }
            estado1=1;
            ver1=1;
            delay(25);
            digitalWrite(rele1, LOW);
          }
        }          
        else
        {
          if(ver1 == 0){
            estado1=0;
            ver1=1;
            delay(25);
            digitalWrite(rele1, HIGH);
          }
        }          
      }
  }
  else
  {    
    ver1=0;
    filtro1=100;
  }
//==========================================================

//Logica do Botao 2===========================================
  if (digitalRead(botao2) == 0 || but2 == 1)
  {    
      filtro2--;
      if(filtro2==0)
      {
        digitalWrite(rele1, HIGH);
        digitalWrite(rele3, HIGH);
        if(estado2==0)
        {
          if(ver2 == 0){
            //Condicional caso o botao 1 ou 3 estejam ativos
            if(estado1 == 1 || estado3 == 1)
            {
              //Desliga ambos
              estado1 = 0;
              estado3 = 0;
              digitalWrite(rele1, HIGH);
              digitalWrite(rele3, HIGH);
            }
            estado2=1;
            ver2=1;
            delay(25);
            digitalWrite(rele2, LOW);
          }
        }          
        else
        {
          if(ver2 == 0){
            estado2=0;
            delay(25);
            digitalWrite(rele2, HIGH);
            ver2=1;
          }
        }          
      }
  }
  else
  {
    ver2=0;
    filtro2=100;
  }
//==========================================================

//Logica do Botao 3===========================================
  if (digitalRead(botao3) == 0 || but3 == 1)
  {    
      filtro3--;
      if(filtro3==0)
      {
        digitalWrite(rele1, HIGH);
        digitalWrite(rele2, HIGH);
        if(estado3==0)
        {
          if(ver3 == 0){
            //Condicional caso o botao 1 ou 2 estejam ativos
            if(estado1 == 1 || estado2 == 1)
            {
              //Desliga ambos
              estado1 = 0;
              estado2 = 0;
              digitalWrite(rele1, HIGH);
              digitalWrite(rele2, HIGH);
            }
            estado3=1;
            ver3=1;
            delay(25);
            digitalWrite(rele3, LOW);
          }    
        }      
        else
        {
          if(ver3 == 0){
            estado3=0;
            ver3=1;
            delay(25);
            digitalWrite(rele3, HIGH);
          }
        }          
      }
  }
  else
  {
    ver3=0;
    filtro3=100;
  }
//==========================================================
    
//Logica do Botao 4===========================================
  if (digitalRead(botao4) == 0 || but4 == 1)
  {    
      filtro4--;
      if(filtro4==0)
      {     
           
        if(estado4==0)
        {
          if(ver4 == 0){
            estado4=1;
            ver4=1;
            digitalWrite(rele4, LOW);
          }
        }          
        else
        {
          if(ver4 == 0){
          estado4=0;
          ver4=1;        
          digitalWrite(rele4, HIGH);
          }
        }          
      }
  }
  else
  {
    ver4=0;
    filtro4=100;
  }
//==========================================================

  modbusino_slave.loop(tab_reg, 10);


}
